(ns cljs-compiler-demo.core
    (:require
     [reagent.core :as r]
     [cljs.js :as cljsjs]))

;; -------------------------
;; Views

(def cols "50")

(def rows "10")

;; states
(defonce cljs-code (r/atom "(println \"hello\")"))

(defonce compiled (r/atom ""))

(cljsjs/compile-str (cljsjs/empty-state) "(println 1)" println)

(defn compiled-callback
  [x]
  (if (:value x)
    (reset! compiled (:value x))
    (reset! compiled (:error x))))

(defn home-page []
  [:div
   [:div
    [:h3 "Your CLJS Code Here"]
    [:textarea {:id "your-code" :cols cols :rows rows :on-change #(reset! cljs-code (-> % .-target .-value)) :value @cljs-code}]]
   [:div
    [:button {:id "compile-button"  :on-click #(cljsjs/compile-str (cljsjs/empty-state) @cljs-code compiled-callback)} "Compile"]]
   [:div
    [:h3 "Compiled Code Here"]
    [:textarea {:id "compiled-code" :cols cols :rows rows :value @compiled}]]])

;; -------------------------
;; Initialize app

(defn mount-root []
  (r/render [home-page] (.getElementById js/document "app")))

(defn init! []
  (mount-root))

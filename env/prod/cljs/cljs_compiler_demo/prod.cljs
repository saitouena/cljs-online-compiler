(ns cljs-compiler-demo.prod
  (:require
    [cljs-compiler-demo.core :as core]))

;;ignore println statements in prod
(set! *print-fn* (fn [& _]))

(core/init!)
